﻿using SUST_REST.Logics.Interfaces;
using SUST_REST.Models;
using System;
using System.Collections.Generic;

namespace SUST_REST.Logics
{
    public class SustitutionFlow : ISustitutionFlow
    {
        private readonly IMayusControl _mayusControl;

        public SustitutionFlow(IMayusControl mayusControl)
        {
            this._mayusControl = mayusControl;
        }

        public string sustitution(string origen, List<Rule> rulesList)
        {
            Char[] converter = origen.ToCharArray();
            List<Char> rulesApplies = new List<Char>();

            foreach (Rule rule in rulesList)
            {
                if (!rulesApplies.Contains(rule.origen))
                {
                    for (int i = 0; i < converter.Length; i++)
                    {
                        if (rule.distinguirMayusculasAlSustituir == true)
                        {
                            converter[i] = _mayusControl.caseSensitive(rule.origen, rule.destino, converter[i]);
                        }
                        else
                        {
                            converter[i] = _mayusControl.caseNoSensitive(rule.origen, rule.destino, converter[i]);
                        }
                    }
                    rulesApplies.Add(rule.origen);
                }

            }

            String result = new String(converter);
            return result;
        }
    }
}
