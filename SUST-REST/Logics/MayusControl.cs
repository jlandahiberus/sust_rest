﻿using SUST_REST.Logics.Interfaces;
using System;

namespace SUST_REST.Logics
{
    public class MayusControl : IMayusControl
    {
        public char caseSensitive(char o, char d, char converter)
        {
            return (o == converter) ? d : converter;
        }

        public char caseNoSensitive(char o, char d, char converter)
        {
            if (Char.IsUpper(o))
            {
                if (o == Char.ToUpper(converter))
                {
                    return d;
                }
                else
                {
                    return converter;
                }
            }
            else
            {
                if (o == Char.ToLower(converter))
                {
                    return d;
                }
                else
                {
                    return converter;
                }
            }
        }
    }
}
