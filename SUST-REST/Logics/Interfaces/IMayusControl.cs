﻿
namespace SUST_REST.Logics.Interfaces
{
    public interface IMayusControl
    {
        char caseSensitive(char o, char d, char converter);

        char caseNoSensitive(char o, char d, char converter);
    }
}
