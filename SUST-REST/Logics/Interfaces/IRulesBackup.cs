﻿using System;

namespace SUST_REST.Logic.Interfaces
{
    public interface IRulesBackup
    {
        void WriteRules(string rules);

        String ReadRules();
    }
}
