﻿
using SUST_REST.Models;
using System.Collections.Generic;

namespace SUST_REST.Logics.Interfaces
{
    public interface ISustitutionFlow
    {
        string sustitution(string origen, List<Rule> rulesList);
    }
}
