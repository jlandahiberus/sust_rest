﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SUST_REST.Configurations.Interfaces;
using SUST_REST.Logic.Interfaces;
using SUST_REST.Models;
using System;
using System.IO;

namespace SUST_REST.Logic
{
    public class RulesBackup : IRulesBackup
    {

        private readonly IFileConfiguration _config;
        private readonly ILogger<RulesBackup> _logger;

        public RulesBackup(IFileConfiguration config, ILogger<RulesBackup> logger)
        {
            this._config = config;
            this._logger = logger;
        }

        public void WriteRules(string rules)
        {
            string dir = _config.getFileConfiguration().directory;

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            dir = dir + _config.getFileConfiguration().file;

            _logger.LogInformation("Writting rules in backup file: " + dir);
            using (StreamWriter writer = File.CreateText(dir))
            {
                writer.Write(rules);
                _logger.LogInformation("Rules Writted ==>" + rules);
            }
        }

        public String ReadRules()
        {
            string dir = _config.getFileConfiguration().directory + _config.getFileConfiguration().file;

            _logger.LogInformation("Reading rules from backup file: " + dir);
            if (File.Exists(dir))
            {
                return File.ReadAllText(dir);
            }
            else
            {
                _logger.LogWarning("Backup file doesn't exist, Rules can't be load");
                return "error";
            }
        }
    }
}
