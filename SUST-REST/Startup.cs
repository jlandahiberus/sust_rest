﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;
using SUST_REST.Configurations;
using SUST_REST.Configurations.Interfaces;
using SUST_REST.Logic;
using SUST_REST.Logic.Interfaces;
using SUST_REST.Logics;
using SUST_REST.Logics.Interfaces;
using SUST_REST.Services;
using SUST_REST.Services.Interfaces;


namespace SUST_REST
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IMayusControl, MayusControl>();
            services.AddScoped<IRulesBackup, RulesBackup>();
            services.AddScoped<ISustitutionFlow, SustitutionFlow>();
            services.AddScoped<IapiClientService, apiClientService>();
            services.AddScoped<ISustitutionService, SustitutionService>();
            services.AddScoped<IFileConfiguration, FileConfiguration>();
            services.AddScoped<IServiceConfiguration, ServiceConfiguration>();
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            env.ConfigureNLog("nlog.config");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            loggerFactory.AddNLog();
            app.AddNLogWeb();

            app.UseMvc();
        }
    }
}
