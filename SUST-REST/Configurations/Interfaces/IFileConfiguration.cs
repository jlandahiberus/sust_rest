﻿using SUST_REST.Models;

namespace SUST_REST.Configurations.Interfaces
{
    public interface IFileConfiguration
    {
        FileConfig getFileConfiguration();
    }
}
