﻿using Microsoft.Extensions.Configuration;
using SUST_REST.Configurations.Interfaces;
using SUST_REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SUST_REST.Configurations
{
    public class FileConfiguration : IFileConfiguration
    {
        private readonly IConfiguration _configuration;
        private FileConfig _fileConfig = new FileConfig();

        public FileConfiguration(IConfiguration configuration)
        {
            this._configuration = configuration;
            this._fileConfig.directory = _configuration.GetSection("BackupRules:Dir").Value;
            this._fileConfig.file = _configuration.GetSection("BackupRules:File").Value;
        }

        public FileConfig getFileConfiguration()
        {
            return _fileConfig;
        }
    }
}
