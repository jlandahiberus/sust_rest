﻿using Microsoft.Extensions.Configuration;
using SUST_REST.Configurations.Interfaces;
using SUST_REST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SUST_REST.Configurations
{
    public class ServiceConfiguration : IServiceConfiguration
    {
        private readonly IConfiguration _configuration;
        private ServiceConfig _serviceConfig = new ServiceConfig();

        public ServiceConfiguration(IConfiguration configuration)
        {
            this._configuration = configuration;
            this._serviceConfig.contentType = _configuration.GetSection("RulesService:Content-Type").Value;
            this._serviceConfig.url = _configuration.GetSection("RulesService:Route").Value;
            this._serviceConfig.endPoint = _configuration.GetSection("RulesService:endPoint").Value;
        }

        public ServiceConfig getServiceConfiguration()
        {
            return this._serviceConfig;
        }
    }
}
