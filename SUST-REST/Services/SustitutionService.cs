﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SUST_REST.Logic.Interfaces;
using SUST_REST.Logics.Interfaces;
using SUST_REST.Models;
using SUST_REST.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SUST_REST.Services
{
    public class SustitutionService : ISustitutionService
    {
        private readonly IapiClientService _apiService;
        private readonly ISustitutionFlow _sustitutionFlow;
        private readonly IRulesBackup _rulesBackup;
        private readonly ILogger<SustitutionService> _logger;

        public SustitutionService(IapiClientService apiService, ISustitutionFlow sustitutionFlow, IRulesBackup rulesBackup, ILogger<SustitutionService> logger)
        {
            this._apiService = apiService;
            this._sustitutionFlow = sustitutionFlow;
            this._rulesBackup = rulesBackup;
            this._logger = logger;

        }

        public async Task<IActionResult> callService(Sustitution value)
        {
            var responseString = await _apiService.callRulesService();
            _logger.LogInformation("Connection Result. Result ==>" + responseString);

            if (!responseString.Equals("error"))
            {
                _rulesBackup.WriteRules(responseString);
                var listRules = JsonConvert.DeserializeObject<List<Rule>>(responseString);
                string toConvert = value.Original;
                value.Convertida = _sustitutionFlow.sustitution(toConvert, listRules);
                _logger.LogInformation("Sustitution Success. Result ==>" + value.Convertida);
                return new OkObjectResult(value);
            }
            else
            {
                var backupString = _rulesBackup.ReadRules();
                if (!backupString.Equals("error"))
                {
                    var listRules = JsonConvert.DeserializeObject<List<Rule>>(backupString);
                    string toConvert = value.Original;
                    value.Convertida = _sustitutionFlow.sustitution(toConvert, listRules);
                    _logger.LogInformation("Sustitution Success. Result ==>" + value.Convertida);
                    return new BadRequestObjectResult(value);
                }
                else
                {
                    return new BadRequestResult();
                }
            }
        }
    }
}
