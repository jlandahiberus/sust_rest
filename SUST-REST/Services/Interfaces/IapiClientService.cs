﻿using System;
using System.Threading.Tasks;

namespace SUST_REST.Services.Interfaces
{
    public interface IapiClientService
    {
        Task<String> callRulesService();
    }
}
