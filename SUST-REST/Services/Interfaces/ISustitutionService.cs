﻿using Microsoft.AspNetCore.Mvc;
using SUST_REST.Models;
using System.Threading.Tasks;

namespace SUST_REST.Services.Interfaces
{
    public interface ISustitutionService
    {
        Task<IActionResult> callService(Sustitution value);
    }
}
