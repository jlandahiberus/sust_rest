﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SUST_REST.Configurations.Interfaces;
using SUST_REST.Services.Interfaces;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace SUST_REST.Services
{
    public class apiClientService : IapiClientService
    {
        private readonly IServiceConfiguration _config;
        private readonly ILogger<apiClientService> _logger;

        public apiClientService(IServiceConfiguration config, ILogger<apiClientService> logger)
        {
            this._config = config;
            this._logger = logger;
        }

        public async Task<String> callRulesService()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue(_config.getServiceConfiguration().contentType));

            String connectionString = _config.getServiceConfiguration().url + _config.getServiceConfiguration().endPoint;

            _logger.LogInformation("Trying to connect : " + connectionString);
            try
            {
                var response = await client.GetAsync(connectionString, HttpCompletionOption.ResponseHeadersRead);

                if (response.IsSuccessStatusCode)
                {
                    _logger.LogInformation("Connection established | Trying to GET rules");
                    var json = await response.Content.ReadAsStringAsync();

                    _logger.LogInformation("Connection Result ==> " + json);
                    return json;
                }
                else
                {
                    _logger.LogWarning("Connection Error | Error return to load Backup rules");
                    return "error";
                }
            }catch(Exception e)
            {
                _logger.LogWarning("Connection Error | Error return to load Backup rules", e);
                return "error";
            }

        }
    }
}
