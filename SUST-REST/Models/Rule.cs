﻿
namespace SUST_REST.Models
{
    public class Rule
    {
        public char origen { get; set; }
        public char destino { get; set; }
        public bool distinguirMayusculasAlSustituir { get; set; }
    }
}
