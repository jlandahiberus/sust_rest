﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SUST_REST.Models
{
    public class FileConfig
    {
        public string directory { get; set; }
        public string file { get; set; }
    }
}
