﻿
namespace SUST_REST.Models
{
    public class Sustitution
    {
        public string Original { get; set; }
        public string Convertida { get; set; }
    }
}
