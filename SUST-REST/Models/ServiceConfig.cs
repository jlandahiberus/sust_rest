﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SUST_REST.Models
{
    public class ServiceConfig
    {
        public string url { get; set; }
        public string endPoint { get; set; }
        public string contentType { get; set; }
    }
}
