﻿using Microsoft.AspNetCore.Mvc;
using SUST_REST.Models;
using System.Threading.Tasks;
using SUST_REST.Services.Interfaces;
using Microsoft.Extensions.Logging;

namespace SUST_REST.Controllers
{
    [Route("api/[controller]")]
    public class SustitutionController : Controller
    {
        private readonly ISustitutionService _sustitutionService;
        private readonly ILogger<SustitutionController> _logger;
        
        public SustitutionController(ISustitutionService sustitutionService, ILogger<SustitutionController> logger)
        {
            this._sustitutionService = sustitutionService;
            this._logger = logger;
        }

        // POST api/sustitution
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Sustitution value)
        {
            _logger.LogInformation("Post Request accepted ==> " + value.Original);
            return await _sustitutionService.callService(value);
        }
    }
}
