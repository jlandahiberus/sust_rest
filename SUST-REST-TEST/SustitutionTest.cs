using SUST_REST.Logics;
using SUST_REST.Models;
using System.Collections.Generic;
using Xunit;

namespace SUST_REST_TEST
{
    public class SustitutionTest
    {
        [Fact]
        public void ApplyRules()
        {
            MayusControl mayusControl = new MayusControl();

            SustitutionFlow sustitutionService = new SustitutionFlow(mayusControl);

            List<Rule> list = new List<Rule>();
            Rule rule = new Rule { origen = 'o', destino = 'a', distinguirMayusculasAlSustituir = true };
            list.Add(rule);
            Rule ruleMayus = new Rule { origen = 'T', destino = 'D', distinguirMayusculasAlSustituir = false };
            list.Add(ruleMayus);
            Rule ruleMinus = new Rule { origen = 'N', destino = 'z', distinguirMayusculasAlSustituir = false };
            list.Add(ruleMinus);
            Rule ruleSens = new Rule { origen = 'E', destino = 't', distinguirMayusculasAlSustituir = true };
            list.Add(ruleSens);
            Rule ruleSensMayus = new Rule { origen = 'P', destino = 'x', distinguirMayusculasAlSustituir = true };
            list.Add(ruleSensMayus);
            Rule rulePos = new Rule { origen = 'o', destino = 'w', distinguirMayusculasAlSustituir = true };
            list.Add(rulePos);

            string response = sustitutionService.sustitution("Hola esto es un test de Probatina", list);

            Assert.Equal("Hala esDa es uz DesD de xrabaDiza", response);
        }
    }
}
