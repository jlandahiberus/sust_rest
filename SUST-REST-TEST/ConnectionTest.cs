﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using SUST_REST.Configurations;
using SUST_REST.Services;
using System.Threading.Tasks;
using Xunit;

namespace SUST_REST_TEST
{
    public class ConnectionTest
    {
        [Fact]
        public async Task ConnectionErrorStatus()
        {
            Mock<ILogger<apiClientService>> moqLogger = new Mock<ILogger<apiClientService>>();
            var config = new ConfigurationBuilder().AddJsonFile("appsettings-test.json").Build();
            ServiceConfiguration serviceConfig = new ServiceConfiguration(config);

            apiClientService service = new apiClientService(serviceConfig, moqLogger.Object);

            string result = await service.callRulesService();
            Assert.Equal("error", result);
        }
    }
}
