﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using SUST_REST.Configurations;
using SUST_REST.Logic;
using System;
using System.IO;
using Xunit;

namespace SUST_REST_TEST
{
    public class BackupTest
    {
        [Fact]
        public void BackupFile()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings-test.json").Build();
            Mock<ILogger<RulesBackup>> moqLogger = new Mock<ILogger<RulesBackup>>();
            FileConfiguration fileConfig = new FileConfiguration(config);


            RulesBackup backup = new RulesBackup(fileConfig, moqLogger.Object);
            var dir = fileConfig.getFileConfiguration().directory;
            if (Directory.Exists(dir))
            {
                Directory.Delete(dir, true);
            }

            string escritura = "Prueba de escritura";
            backup.WriteRules(escritura);
            string lectura = backup.ReadRules();

            Assert.Equal(escritura, lectura);
        }

        [Fact]
        public void BacupNoExist()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings-test.json").Build();
            Mock<ILogger<RulesBackup>> moqLogger = new Mock<ILogger<RulesBackup>>();
            FileConfiguration fileConfig = new FileConfiguration(config);

            RulesBackup backup = new RulesBackup(fileConfig, moqLogger.Object);
            
            var file = fileConfig.getFileConfiguration().directory + fileConfig.getFileConfiguration().file;
            if (File.Exists(file))
            {
                File.Delete(file);
            }

            string lectura = backup.ReadRules();
            Assert.Equal("error", lectura);
        }
    }
}
