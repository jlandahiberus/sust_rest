﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using SUST_REST.Controllers;
using SUST_REST.Models;
using SUST_REST.Services.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace SUST_REST_TEST
{
    public class ControllerTest
    {
        [Fact]
        public void TryController()
        {
            Mock<ISustitutionService> mockService = new Mock<ISustitutionService>();

            mockService.Setup(x => x.callService(It.IsAny<Sustitution>()))
                .Returns(Task.FromResult(new OkObjectResult(new Sustitution { Original = "original" }) as IActionResult));

            Mock<ILogger<SustitutionController>> moqLogger = new Mock<ILogger<SustitutionController>>();

            SustitutionController controller = new SustitutionController(mockService.Object, moqLogger.Object);
            var response = controller.Post(new Sustitution { Original = "original" }).Result as OkObjectResult;
            var result = response.Value as Sustitution;

            Assert.Equal(result.Original, "original");
        }
    }
}
