﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using SUST_REST.Logic.Interfaces;
using SUST_REST.Logics;
using SUST_REST.Models;
using SUST_REST.Services;
using SUST_REST.Services.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace SUST_REST_TEST
{
    public class SustitutionServiceTest
    {
        [Fact]
        public void TryService()
        {

            Mock<IapiClientService> mockApiClient = new Mock<IapiClientService>();
            mockApiClient.Setup(x => x.callRulesService()).Returns(
                Task.FromResult("[{\"origen\":\"a\", \"destino\":\"O\", \"distinguirMayusculasAlSustituir\":false}]"));
            Mock<ILogger<SustitutionService>> moqLogger = new Mock<ILogger<SustitutionService>>();
            MayusControl mayusControl = new MayusControl();
            SustitutionFlow flow = new SustitutionFlow(mayusControl);
            Mock<IRulesBackup> mockBackup = new Mock<IRulesBackup>();
            mockBackup.Setup(x => x.WriteRules("Ok"));
            SustitutionService sustitutionService = new SustitutionService(mockApiClient.Object, flow, mockBackup.Object, moqLogger.Object);
            Sustitution body = new Sustitution { Original = "Esto es una prueba"};

            var response = sustitutionService.callService(body).Result as OkObjectResult;
            var result = response.Value as Sustitution;

            Assert.Equal("Esto es unO pruebO", result.Convertida);
        }


        [Fact]
        public void TryControllerBackUp()
        {

            Mock<IapiClientService> mockApiClient = new Mock<IapiClientService>();
            mockApiClient.Setup(x => x.callRulesService()).Returns(Task.FromResult("error"));
            Mock<ILogger<SustitutionService>> moqLogger = new Mock<ILogger<SustitutionService>>();
            MayusControl mayusControl = new MayusControl();
            SustitutionFlow flow = new SustitutionFlow(mayusControl);
            Mock<IRulesBackup> mockBackup = new Mock<IRulesBackup>();
            mockBackup.Setup(x => x.ReadRules()).Returns("[{\"origen\":\"a\", \"destino\":\"O\", \"distinguirMayusculasAlSustituir\":false}]");
            SustitutionService sustitutionService = new SustitutionService(mockApiClient.Object, flow, mockBackup.Object, moqLogger.Object);
            Sustitution body = new Sustitution { Original = "Esto es una prueba" };

            var response = sustitutionService.callService(body).Result as BadRequestObjectResult;
            var result = response.Value as Sustitution;

            Assert.Equal("Esto es unO pruebO", result.Convertida);
        }

        [Fact]
        public void TryControllerErrors()
        {

            Mock<IapiClientService> mockApiClient = new Mock<IapiClientService>();
            mockApiClient.Setup(x => x.callRulesService()).Returns(Task.FromResult("error"));
            Mock<ILogger<SustitutionService>> moqLogger = new Mock<ILogger<SustitutionService>>();

            Mock<ISustitutionService> mockSustitution = new Mock<ISustitutionService>();
            MayusControl mayusControl = new MayusControl();
            SustitutionFlow flow = new SustitutionFlow(mayusControl);
            Mock<IRulesBackup> mockBackup = new Mock<IRulesBackup>();
            mockBackup.Setup(x => x.ReadRules()).Returns("error");
            SustitutionService sustitutionService = new SustitutionService(mockApiClient.Object, flow, mockBackup.Object, moqLogger.Object);

            Sustitution body = new Sustitution { Original = "Esto es una prueba" };
            var response = sustitutionService.callService(body).Result;
            var result = response as BadRequestResult;
            
            Assert.Equal(400, result.StatusCode);
        }
    }
}
